class Chromosome:
    def __init__(self, coefs, free_term, fitness=0):
        self.coefs = coefs
        self.free_term = free_term
        self.fitness = fitness

    def __str__(self):
        return str(self.coefs) + " " + str(self.free_term)