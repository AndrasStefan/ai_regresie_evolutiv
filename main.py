import csv
import random
from chromo import Chromosome
from utils import print_green

MOTOR_UDPRS = False
TOTAL_UDPARS = not MOTOR_UDPRS


def read_from_csv(path):
    """
    :param path: path to the file
    :return: X, Y -> list of lists (o lista pentru fiecare pacient)
    """
    x, y = [], []
    index = 4 if MOTOR_UDPRS else 5

    with open(path) as csvfile:
        reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            x.append(row[6:])
            y.append(row[index])

    return x, y


INPUT_FILE = "parkinsons_updrs.data.txt"
POPULATION_SIZE = 10
NUMBER_OF_GENERATIONS = 100
MUTATION_CONSTANT = 0.5
x, y = read_from_csv(INPUT_FILE)


def solve():
    population = initialization()
    fitness(population)

    for generation in range(NUMBER_OF_GENERATIONS):
        pop_aux = []
        for _ in range(POPULATION_SIZE):
            m = selection(population)
            f = selection(population)
            new = crossover(m, f)
            mutation(new)
            pop_aux.append(new)

        population = pop_aux
        fitness(population)
        print(str(generation) + " " + str(min(population, key=lambda chromo: chromo.fitness).fitness))

    return min(population, key=lambda chromo: chromo.fitness)


def initialization():
    return [Chromosome([random.random() for _ in range(16)], random.random(), 0)
            for _ in range(POPULATION_SIZE)]


def fitness(pop):
    for indiv in pop:
        indiv.fitness = _fitness_individ(indiv)


def _fitness_individ(chromo):
    error = 0
    for measures, udprs in zip(x, y):
        one_line = sum(meas * coef for meas, coef in zip(measures, chromo.coefs)) + chromo.free_term
        error += (one_line - udprs) ** 2
    return error


def selection(pop):
    m, f = random.choice(pop), random.choice(pop)
    return m if m.fitness < f.fitness else f


def crossover(m, f):
    new_coefs = [(coefm + coeff) / 2 for coefm, coeff in zip(m.coefs, f.coefs)]
    return Chromosome(new_coefs, (m.free_term + f.free_term) / 2, 0)


def mutation(chromo, e=MUTATION_CONSTANT):
    sign = random.choice('+-')
    index = random.randrange(16)

    if sign == "+":
        chromo.coefs[index] += e
    else:
        chromo.coefs[index] -= e


def error(coefs, free_term):
    measures, result = read_from_csv('data_from_user.txt')
    rez = sum(meas * coef for meas, coef in zip(measures[0], coefs)) + free_term
    return rez - result[0]


# TODO: normalizare date
if __name__ == "__main__":
    best = solve()
    print(best.fitness, best)
    print_green(str(error(best.coefs, best.free_term)))
